import React from "react";
import "./ProgressBar.scss";

const ProgressBar = (props) => {
  const { value, max, className } = props;
  const classNameResult = `Progress Progress--small overflow-hidden anim-scale-in ${
    className ? className : ``
  }`;

  return (
    <>
      <span className={classNameResult}>
        <span
          className="bg-blue"
          style={{ width: `${parseFloat((100 / max) * value).toFixed(3)}%` }}
        ></span>
      </span>
    </>
  );
};

export default ProgressBar;
